#!/usr/bin/python3

import os
os.putenv('SDL_AUDIODRIVER', 'alsa')
os.putenv('SDL_AUDIODEV', '/dev/audio')
import time
import datetime
import random
import pygame   # Needs Pygame installed
import io
import sys
import builtins
import traceback
import subprocess
import configparser
from subprocess import check_output, call
from kivy.app import App
from kivy.clock import Clock
from kivy.properties import NumericProperty, StringProperty, ObjectProperty, BoundedNumericProperty
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.popup import Popup
from kivy.config import Config
from functools import partial

class Screen1(Screen):
    pass

class Screen2(Screen):
    pass

class Screen3(Screen):
    pass

class Screen4(Screen):
    pass

class DigitalClock(ScreenManager):
	mixer = str('')
	secs_to_next_minute = int(0)
	display_time = StringProperty("00 : 00")
	clock_view = NumericProperty(1)        # Variable to enable view of the Clock Time
	advanced_view = NumericProperty(0)
	settings_pic = StringProperty("./Images/Buttons/Settings_Normal.png")
	is_snoozing = NumericProperty(0)       # Track when in Snooze mode
	alarm_time_hour = BoundedNumericProperty(0, min=1, max=24, errorhandler=lambda x: 1 if x > 24 else 24)
	alarm_time_minute = BoundedNumericProperty(0, min=0, max=59, errorhandler=lambda x: 0 if x > 59 else 59)
	alarm_time = StringProperty('0')
	snooze_time = BoundedNumericProperty(0, min=1, max=10, errorhandler=lambda x: 1 if x > 10 else 10)
	current_weekday = StringProperty('0')
	set_sunday = StringProperty('0')
	set_monday = StringProperty('0')
	set_tuesday = StringProperty('0')
	set_wednesday = StringProperty('0')
	set_thursday = StringProperty('0')
	set_friday = StringProperty('0')
	set_saturday = StringProperty('0')
	set_weekday = ObjectProperty()
	alarm_switch = NumericProperty(0)
	switch_text = StringProperty('OFF')
	hr_setting = NumericProperty(12)
	hr_text = StringProperty('24HR')
	am_pm_clock = NumericProperty(0)
	am_pm_clock_text = StringProperty('AM')
	am_pm_setting = NumericProperty('0')
	am_pm_text = StringProperty('AM')
	nfc_cap = str('3e')
	nfc_cap2 = str('4>')
	nfc_hulk = str('48')
	nfc_hulk2 = str('4H')
	nfc_locked = False
	colour = NumericProperty(0)
	curr_time = int
	curr_day_name = StringProperty('')  
	alarm_event = ()
	nfc_checking = int(0)
	section = int(0)            # Section of audio files to use 1=1-7, 2=8-14, 3=15-21
	audio_path = str('')
	audio = int(0)
	curr_vol = NumericProperty(50)
	label_vol = StringProperty('0%')
	alarm_vol = NumericProperty(95)
	sys_year = StringProperty('0%')
	sys_month = StringProperty('0%')
	sys_day = StringProperty('0%')
	sys_hour = StringProperty('0%')
	sys_minute = StringProperty('0%')
	sys_second = StringProperty('0%')
	param = {}
	pygame.init()


	def startup(self):
		config_mod = False
		config = configparser.ConfigParser()
		if not os.path.exists('config.ini'):
			write_file()
		config.read('config.ini')
		try:
			self.alarm_time_hour = int(config['Alarm Time']['alarm_time_hour'])
		except:
			config_mod = True
		try:
			self.alarm_time_minute = int(config['Alarm Time']['alarm_time_minute'])
		except:
			config_mod = True
		finally:
			self.alarm_time = (str(self.alarm_time_hour).zfill(2) + " : " + str(self.alarm_time_minute).zfill(2))
		try:
			self.set_sunday = (config['Alarm Days']['set_sunday'])
		except:
			config_mod = True
		try:
			self.set_monday = (config['Alarm Days']['set_monday'])
		except:
			config_mod = True
		try:
			self.set_tuesday = (config['Alarm Days']['set_tuesday'])
		except:
			config_mod = True
		try:
			self.set_wednesday = (config['Alarm Days']['set_wednesday'])
		except:
			config_mod = True
		try:
			self.set_thursday = (config['Alarm Days']['set_thursday'])
		except:
			config_mod = True
		try:
			self.set_friday = (config['Alarm Days']['set_friday'])
		except:
			config_mod = True
		try:
			self.set_saturday = (config['Alarm Days']['set_saturday'])
		except:
			config_mod = True
		try:
			self.alarm_switch = (config['Miscellaneous']['alarm_switch'])
		except:
			config_mod = True
		try:
			self.switch_text = (config['Miscellaneous']['switch_text'])
		except:
			config_mod = True
		try:
			self.hr_setting = (config['Miscellaneous']['hr_setting'])
		except:
			config_mod = True
		try:
			self.hr_text = (config['Miscellaneous']['hr_text'])
		except:
			config_mod = True
		try:
			self.am_pm_clock = (config['Miscellaneous']['am_pm_clock'])
		except:
			config_mod = True
		try:
			self.am_pm_clock_text = (config['Miscellaneous']['am_pm_clock_text'])
		except:
			config_mod = True
		try:
			self.am_pm_setting = (config['Miscellaneous']['am_pm_setting'])
		except:
			config_mod = True
		try:
			self.am_pm_text = (config['Miscellaneous']['am_pm_text'])
		except:
			config_mod = True
		try:
			self.snooze_time = int(config['Miscellaneous']['snooze_time'])
			if self.snooze_time == 0:
				self.snooze_time = 5
		except:
			config_mod = True
		try:
			self.curr_vol = (config['Miscellaneous']['curr_vol'])
		except:
			config_mod = True
		try:
			self.alarm_vol = (config['Miscellaneous']['alarm_vol'])
		except:
			config_mod = True
		try:
			self.nfc_cap = (config['Character IDs']['nfc_cap'])
		except:
			config_mod = True
		try:
			self.nfc_cap2 = (config['Character IDs']['nfc_cap2'])
		except:
			config_mod = True
		try:
			self.nfc_hulk = (config['Character IDs']['nfc_hulk'])
		except:
			config_mod = True
		try:
			self.nfc_hulk2 = (config['Character IDs']['nfc_hulk2'])
		except:
			config_mod = True
		if config_mod == True:
			self.save_config()
		self.mixer = check_output('amixer', universal_newlines=True)
		if ("'Speaker'" in self.mixer):
			self.mixer = "'Speaker'"
		elif ("'PCM'" in self.mixer):
			self.mixer = "'PCM'"
		elif ("'Maser'" in self.mixer):
			self.mixer = "'Master'"
		else: 
			self.mixer = "'UNKNOWN'"
		os.system ("amixer sset " + self.mixer + " " + str(self.curr_vol) + "%")
		subprocess.Popen(['gmediarender', '--gstout-audiosink=alsasink'])
		self.update()

	def update(self, dt=0):
		Clock.unschedule(self.update)    # attempt to fix memory leak - Attempt Successful :)
		if self.hr_setting == 0:
			self.display_time = time.strftime("%H : %M")
		else:
			if self.hr_setting == 12:
				self.display_time = time.strftime("%I : %M")
		self.sys_year = time.strftime("%Y")
		self.sys_month = time.strftime("%B")
		self.sys_day = time.strftime("%d")
		if self.hr_setting == 0:
			self.sys_hour = time.strftime("%H")
		else:
			self.sys_hour = time.strftime("%I")
		self.sys_minute = time.strftime("%M")
		self.am_pm_clock_text = time.strftime("%p")
		current_time = time.localtime()
		self.current_weekday = str(datetime.datetime.today().isoweekday() %7 + 1)
		seconds = current_time[5]
		self.curr_time = datetime.datetime.now()
		#self.curr_day_name = self.curr_time.strftime("%A") + " - " + self.curr_time.strftime("%B") + " " + self.curr_time.strftime("%-d")
		self.curr_day_name = self.curr_time.strftime("%A")
		if ((self.display_time == self.alarm_time) #If alarm time is equal to current time
		and (self.alarm_switch == 1) #and alarm switch is ON
		and (self.current != "Alarm") 
		and (self.is_snoozing == 0)): # and current screen is not Alarm screen
			if (self.current_weekday == "1" and self.set_sunday == '1'
			or  self.current_weekday == "2" and self.set_monday == '1' #checking current day of week against if it's enabled below
			or  self.current_weekday == "3" and self.set_tuesday == '1'
			or  self.current_weekday == "4" and self.set_wednesday == '1'
			or  self.current_weekday == "5" and self.set_thursday == '1'
			or  self.current_weekday == "6" and self.set_friday == '1'
			or  self.current_weekday == "7" and self.set_saturday == '1'):
				if ((self.hr_setting == 12)
				and (self.am_pm_text == self.am_pm_clock_text)):
					self.alarm_start()
				else:
					if (self.hr_setting == 0):
						self.alarm_start()
		self.secs_to_next_minute = (60 - seconds)
		Clock.schedule_once(self.update, self.secs_to_next_minute) #Update again in 1 minute

	def alarm_start(self, *args):# Setup the  Initial Alarm Variables
		self.section = 0 # Sets the alarm sounds to start at the first section
		os.system ("amixer sset " + self.mixer + " " + str(self.alarm_vol) + "%")#Set volume to Alarm Volume slider setting
		nfc_read = str()
		try:    # Run the nfc-poll command and get its output
			nfc_read = check_output('nfc-poll', universal_newlines=True) #universal_newlines just makes it easier to read if you decide to Print
		except:
			pass #If there's an error, just move along and try again.
		if (self.nfc_cap in str(nfc_read)  # Determine audio file path based on NFC read
		or self.nfc_cap2 in str(nfc_read)):
			self.audio_path = "/home/pi/Desktop/PyClock/Sounds/01-CaptainAmerica/"
			self.nfc_locked = True
		else:
			if (self.nfc_hulk in str(nfc_read) # Determine audio file path based on NFC read
			or self.nfc_hulk2 in str(nfc_read)):
				self.audio_path = "/home/pi/Desktop/PyClock/Sounds/02-Hulk/"
				self.nfc_locked = True
			else:
				self.audio_path = "/home/pi/Desktop/PyClock/Sounds/" # Default alarm sounds if no NFC tag is found that matches above
		self.current = 'Alarm'#Send to Alarm Screen
		self.alarm_loop()

	def alarm_loop(self, *args):#Loop to run every second while alarming.
		Clock.unschedule(self.alarm_event)      # unschedule the 1 second loop that runs self.alarm_loop
		self.alarm_event = Clock.schedule_interval(self.alarm_loop, 1) #Reschedule for one second. Doing Clock.schduled_once in 1 second intervals had bad results.
		play_num = int(0)
		file_type = str('')
		if self.colour == 0:
			self.colour = 1
		else:
			self.colour = 0
		if pygame.mixer.get_busy() == False:  # Check that audio currently isn't running
			rando = NumericProperty(0)
			if self.audio_path == "/home/pi/Desktop/PyClock/Sounds/":
				self.rando = random.randint(1, 2)
				file_type = '.wav'
				self.section = 0
			else:
				self.rando = random.randint(1, 7)   # Random number between 1 and 7, inclusive
				file_type = '.ogg'
				if self.section == 0:   # Increment to next section every round
					self.section = 7
				else:
					if self.section == 7:
						self.section = 14
					else:
						self.section = 0
			play_num = (self.rando) + (self.section) # My files are split into 3 sections, 1-7, 8-14, 15-21 based on natural progression of intensity
			sound_file = pygame.mixer.Sound(str(self.audio_path + str(play_num) + file_type)) #Load sound file into Pygame
			alarm = pygame.mixer.Sound(sound_file) # Play sound file
			alarm.set_volume(1.0)
			alarm.play()

	def snooze_func(self, *args):
		snooze_secs = int()
		self.is_snoozing = 1
		self.current = 'Time'
		Clock.unschedule(self.alarm_event)      # unschedule the 1 second loop that runs self.alarm_loop					# Change to "Time" Screen
		snooze_secs = self.snooze_time * 60
		self.alarm_event = Clock.schedule_once(self.alarm_start, snooze_secs) # schedule new 5-minute Snooze timer
		self.colour = 0                         # Set background color to Black
		self.nfc_locked = False                # Reset NFC to try again for tag
		if pygame.mixer.get_busy() == True:     # If sounds is currently playing
			pygame.mixer.stop()                 # Stop currently playing sound

	def cancel_func(self, *args):
		Clock.unschedule(self.alarm_event)      # unschedule the 1 second loop that runs self.alarm_loop
		self.is_snoozing = 0
		self.current = 'Time'					# Change to "Time" Screen
		self.colour = 0                         # Set background color to Black
		self.nfc_locked = False                # Reset NFC to try again for tag
		os.system ("amixer sset " + self.mixer + " " + str(self.curr_vol) + "%")
		if pygame.mixer.get_busy() == True:     # If sounds is currently playing
			pygame.mixer.stop()                 # Stop currently playing sound

	def switch_state(self, *args): # Arm/disarm alarm, See button in digitalclock.kv
		if self.alarm_switch == 1:
			self.switch_text = "ALARM ON"
		else:
			self.switch_text = "ALARM OFF"

	def click_12hr(self, *args):#Alternate between 12hr/24hr clock format 12=12 / 0=24
		if self.hr_setting == 0:
			self.hr_setting = 12
			self.hr_text = "12HR"
			self.am_pm_clock = 1
			self.am_pm_clock_text = time.strftime("%p")
			self.display_time = time.strftime("%I : %M")
			self.sys_hour = time.strftime("%I")
			if self.alarm_time_hour > 12:
				self.alarm_time_hour -= 12
				self.am_pm_setting = 1
				self.am_pm_text = "PM"
			if self.alarm_time_hour == 0:
				self.alarm_time_hour = 12
				self.am_pm_setting = 0
				self.am_pm_text = "AM"
			self.alarm_time = str(self.alarm_time_hour).zfill(2) + " : " + str(self.alarm_time_minute).zfill(2) #zfill used for padding single digits numbers with a zero
		else:
			self.hr_setting = 0
			self.display_time = time.strftime("%H : %M")
			self.sys_hour = time.strftime("%H")
			self.hr_text = "24HR"
			self.am_pm_clock = 0

	def alarm_am_pm(self, *args):
		if self.am_pm_setting == 0:
			self.am_pm_setting = 1
			self.am_pm_text = "PM"
		else:
			self.am_pm_setting = 0
			self.am_pm_text = "AM"

	def save_config(self):
		config_file = configparser.ConfigParser()
		config_file['Alarm Time'] = {'alarm_time_hour': str(self.alarm_time_hour),
									 'alarm_time_minute': str(self.alarm_time_minute),
									 }
		config_file['Alarm Days'] = {'set_sunday': str(self.set_sunday),
									 'set_monday': str(self.set_monday),
									 'set_tuesday': str(self.set_tuesday),
									 'set_wednesday': str(self.set_wednesday),
									 'set_thursday': str(self.set_thursday),
									 'set_friday': str(self.set_friday),
									 'set_saturday': str(self.set_saturday),
									 }
		config_file['Miscellaneous'] = {'alarm_switch': str(self.alarm_switch),
										'switch_text': str(self.switch_text),
										'hr_setting': str(self.hr_setting),
										'hr_text': str(self.hr_text),
										'am_pm_clock': str(self.am_pm_clock),
										'am_pm_clock_text': str(self.am_pm_clock_text),
										'am_pm_setting': str(self.am_pm_setting),
										'am_pm_text': str(self.am_pm_text),
										'snooze_time': str(self.snooze_time),
										'curr_vol': str(self.curr_vol),
										'alarm_vol': str(self.alarm_vol),
										}
		config_file['Character IDs'] = {'nfc_cap': str(self.nfc_cap),
										'nfc_cap2': str(self.nfc_cap2),
										'nfc_hulk': str(self.nfc_hulk),
										'nfc_hulk2': str(self.nfc_hulk2)
										}
		with open ('config.ini', 'w') as configfile:
			config_file.write(configfile)

	def alarm_time_update(self):
		if self.hr_setting == 12 and self.alarm_time_hour > 12:
			self.alarm_time_hour = 1
		self.alarm_time = str(self.alarm_time_hour).zfill(2) + " : " + str(self.alarm_time_minute).zfill(2) #zfill used for padding single digits numbers with a zero

	#def system_time_update(self):
		#sudo date --set self.sys_year+"-"+self.sys_month+"-"+self.sys_day+" "+self.sys_hour+":"+str(self.sys_minute)+":00"

	def vol_change(self, value):
		self.curr_vol = value 	# Instantly set system volume output to selected value.
		os.system ("amixer sset " + self.mixer + " " + str(self.curr_vol) + "%")
		print ("Media Volume = " + str(value) + "%")

	def system_config(self):
		os.system ('sudo raspi-config')

class DigitalClockApp(App):
	def build(self):
		dc = DigitalClock()
		dc.startup()

		return dc


if __name__ == '__main__':

	DigitalClockApp().run()
